package main

import (
	"strings"
	"testing"

	"github.com/rendon/testcli"
)

// Helper functions for tests

func testCommand(cmd string, t *testing.T, expectedOutput ...string) {
	s := strings.Split(cmd, " ")
	c := testcli.Command("./a.out", s[0:]...)
	c.Run()

	if !c.Success() {
		t.Fatalf("Command '"+cmd+"' failed with error '%s' and output '%s'.", c.Stderr(), c.Stdout())
	}

	if len(expectedOutput) == 0 && c.Stdout() != "" {
		t.Fatalf("No output anticipated but the following was heard: '" + c.Stdout() + "'.")
	}

	for _, expected := range expectedOutput {
		if !c.StdoutContains(expected) {
			t.Fatalf("Output '" + expected + "' was anticipated but not found in the output '" + c.Stdout() + "'.")
		}
	}
}

func createEnvironment(t *testing.T) {
	testcli.Run("make", "clean")
	testCommand("i Checking Savings", t, "Initialized bank on this computer.")
}

// Tests start here

func TestInitialization(t *testing.T) {
	createEnvironment(t)
}

func TestBalance(t *testing.T) {
	createEnvironment(t)
	testCommand("t add -a Checking Income 500 Me", t)
	testCommand("t add -a Checking Expense 300 Person", t)
	testCommand("b -a Checking", t, "200.00")
}

func TestEditTransaction(t *testing.T) {
	createEnvironment(t)
	testCommand("t add -a Checking Income 250 Me", t)
	testCommand("b -a Checking", t, "250.00")
	testCommand("t edit -a Checking -i 1 --amount 500", t)
	testCommand("b -a Checking", t, "500.00")
}

func TestTransfer(t *testing.T) {
	createEnvironment(t)
	testCommand("t add -a Checking Income 500 Me", t)
	testCommand("t transfer 300 -i Checking -o Savings", t)
	testCommand("b -a Checking", t, "200.00")
	testCommand("b -a Savings", t, "300.00")
}

func TestDefaultAccount(t *testing.T) {
	createEnvironment(t)
	testCommand("setDefault Checking", t)
	testCommand("t add -a Checking Income 200 Me", t)
	testCommand("b", t, "200.00")
	testCommand("b -a Savings", t, "0.00")
}

func TestPiggyBank(t *testing.T) {
	createEnvironment(t)

	testCommand("p a Xbox ExamplePiggyBank 500", t)
	testCommand("t add -p -a Checking SaveUp 300 Xbox", t)
	testCommand("p l Xbox", t, "200.00")

	testCommand("p a SecondOne PiggyBank2 8000", t)
	testCommand("t add -p -a Checking SaveUpMore 7999 SecondOne", t)
	testCommand("p l SecondOne", t, "1.00")
}

func TestBalanceWithoutPiggyBank(t *testing.T) {
	createEnvironment(t)

	testCommand("t add -a Checking Income 500 Me", t)
	testCommand("p a Xbox ExamplePiggyBank 500", t)
	testCommand("t add -p -a Checking SaveUp 300 Xbox", t)
	testCommand("b -a Checking --no-piggy-banks", t, "500.00")
}

func TestDeleteTransaction(t *testing.T) {
	createEnvironment(t)

	testCommand("t add -a Checking Income 500 Me", t)
	testCommand("t add -a Checking IncomeNumberTwo 800 Me", t)
	testCommand("b -a Checking", t, "1300")
	testCommand("t d -a Checking -i 2", t)
	testCommand("b -a Checking", t, "500")
}
