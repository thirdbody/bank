package classes

// PiggyBank represents a budget or virtual account where money can be "moved" to contribute to a certain goal.
type PiggyBank struct {
	Name    string
	Purpose string
	Amount  float64
	Goal    float64
}
