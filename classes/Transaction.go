package classes

import "time"

// Transaction represents a transfer of money out of an account.
type Transaction struct {
	Name         string
	Amount       float64
	Recipient    string
	ForPiggyBank bool
	Date         time.Time
	ID           uint
}
