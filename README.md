# bank

A ledger utility written in Golang.

## Install

Running `make` should be enough to create an executable on your system, provided you have `dep` and `go` installed and that you've cloned this repository into its proper place in your `GOPATH`.

## Usage

Run `bank help` for the help/usage.
