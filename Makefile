test_bank_config_dir = .bank/

build: dep go

dep:
	dep ensure

go:
	go build -o a.out

test: clean dep
	( \
		export BANK_CONFIG_DIR="$(test_bank_config_dir)"; \
		$(MAKE); \
		go test; \
	)

check: test

go_release:
	GOOS=$(GOOS) GOARCH=amd64 go build -o bank-$(GOOS)
	tar -cf - bank-$(GOOS) | xz --stdout -9 - > bank-$(GOOS).tar.xz

release: dep
	$(MAKE) GOOS=darwin go_release
	$(MAKE) GOOS=linux go_release
	$(MAKE) GOOS=windows go_release

clean:
	rm -rf $(test_bank_config_dir) bank-{windows,darwin,linux}*
