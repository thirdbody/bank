package cmd

import (
	"fmt"
	"strconv"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/myl0g/bank/classes"
	"gitlab.com/myl0g/bank/utils"
)

// transactionCmd represents the transaction command
var transactionCmd = &cobra.Command{
	Use:     "transaction [subcommand]",
	Aliases: []string{"t"},
	Short:   "Manage transactions for your accounts",
	Long: `View an account's transactions with 't v'.
	
	Add a transaction with 't add <name> <amount> <payee>'. 
	
	If payee is set to 'Me', the amount will be credited (not debited) from the account.
	
	Add the -p flag to treat the payee as a piggy bank.`,
}

var viewTransactionsCmd = &cobra.Command{
	Use:     "view [-a account]",
	Aliases: []string{"v"},
	Short:   "View all transactions for your account",
	Long:    `View an account's transactions.`,
	Run: func(cmd *cobra.Command, args []string) {
		transactions := utils.FetchAccount(account).Transactions

		for _, transaction := range transactions {
			fmt.Println(transaction.Name)
			fmt.Print("- Amount: ")
			fmt.Println(transaction.Amount)
			fmt.Print("- Recipient: " + transaction.Recipient)
			if transaction.ForPiggyBank {
				fmt.Print(" (piggy bank)")
			}
			fmt.Println("\n- Date: " + transaction.Date.String())
			fmt.Print("- ID: ")
			fmt.Println(transaction.ID)
		}

	},
}

var piggyBankPayee bool

var addTransactionCmd = &cobra.Command{
	Use:     "add [-a account] [-p (optional)] [name] [amount] [payee]",
	Aliases: []string{"a"},
	Short:   "Add a transaction to an account",
	Long: `Add an account transaction. Examples are shown below:
	
	"bank t add -a Checking Income 500 Me" - credit the Checking account 500
	"bank t add -a Checking Expense 300 Person" - debit the Checking account 300`,
	Run: func(cmd *cobra.Command, args []string) {

		amount, err := strconv.ParseFloat(args[1], 64)
		utils.Check(err)

		if args[2] != "Me" {
			amount = -amount
		}

		accnt := utils.FetchAccount(account)

		var id uint
		if len(accnt.Transactions) == 0 {
			id = 1
		} else {
			id = accnt.Transactions[len(accnt.Transactions)-1].ID + 1
		}

		accnt.Transactions = append(accnt.Transactions, classes.Transaction{Name: args[0], Amount: amount, Recipient: args[2], ForPiggyBank: piggyBankPayee, Date: time.Now(), ID: id})

		utils.UpdateAccount(accnt)

		if piggyBankPayee {
			bank := utils.GetPiggyBank(args[2])
			bank.Amount = bank.Amount - amount
			utils.UpdatePiggyBank(bank)
		}
	},
}

var transactionID uint
var newPiggyBankPayee bool
var newName string
var newAmount float64
var newPayee string

var editTransactionCmd = &cobra.Command{
	Use:     "edit [-a account] [-i transactionId] [-p (optional)] [--name name] [--amount amount] [ --payee payee]",
	Aliases: []string{"e"},
	Short:   "Edit a pre-existing transaction by ID number.",
	Long:    `Modify a transaction's data. The -a and -i flags are required, while the rest are optional.`,
	Run: func(cmd *cobra.Command, args []string) {
		accnt := utils.FetchAccount(account)

		var oldTransaction classes.Transaction
		i := -1

		for index, transaction := range accnt.Transactions {
			if transaction.ID == transactionID {
				oldTransaction = transaction
				i = index
				break
			}
		}
		if i == -1 {
			fmt.Println("No transaction with that ID exists.")
			return
		}

		piggy, name, amount, payee := oldTransaction.ForPiggyBank, oldTransaction.Name, oldTransaction.Amount, oldTransaction.Recipient

		if newName != "" {
			name = newName
		}
		if newPayee != "" {
			payee = newPayee
		}
		if newAmount != 0.0 {
			amount = newAmount
			if payee != "Me" {
				amount = -amount
			}
		}
		if newPiggyBankPayee {
			piggy = newPiggyBankPayee
			bank := utils.GetPiggyBank(payee)
			bank.Amount = bank.Amount - amount
			utils.UpdatePiggyBank(bank)
		}

		accnt.Transactions[i] = classes.Transaction{Name: name, Amount: amount, Recipient: payee, ForPiggyBank: piggy, Date: oldTransaction.Date, ID: oldTransaction.ID}
		utils.UpdateAccount(accnt)
	},
}

var deleteTransactionCmd = &cobra.Command{
	Use:     "delete [-a account] [-i transactionId]",
	Aliases: []string{"d"},
	Short:   "Delete a pre-existing transaction by ID number.",
	Long:    `Delete a transaction with the provided ID from the provided account.`,
	Run: func(cmd *cobra.Command, args []string) {
		accnt := utils.FetchAccount(account)

		index := -1
		for i, item := range accnt.Transactions {
			if item.ID == transactionID {
				index = i
				break
			}
		}

		if index == -1 {
			fmt.Println("No item with that transaction ID exists.")
			return
		}

		accnt.Transactions = append(accnt.Transactions[:index], accnt.Transactions[index+1:]...)
		utils.UpdateAccount(accnt)
	},
}

var inputAccount string
var outputAccount string

var transferTransactionCmd = &cobra.Command{
	Use:   "transfer [amount] [-i inputAccount] [-o outputAccount]",
	Short: "Transfer money between two accounts",
	Long: `Transfer money between two accounts.
	
	Example: "bank t transfer 300 -i Checking -o Savings" would debit the Checking account 300 and credit the Savings account 300.`,
	Run: func(cmd *cobra.Command, args []string) {
		amount, err := strconv.ParseFloat(args[0], 64)
		utils.Check(err)

		input := utils.FetchAccount(inputAccount)
		output := utils.FetchAccount(outputAccount)

		var inputID uint
		var outputID uint

		if len(input.Transactions) == 0 {
			inputID = 1
		} else {
			inputID = input.Transactions[len(input.Transactions)-1].ID + 1
		}

		if len(output.Transactions) == 0 {
			outputID = 1
		} else {
			outputID = output.Transactions[len(output.Transactions)-1].ID + 1
		}

		debit := classes.Transaction{Name: "Transfer to " + outputAccount, Amount: -amount, Recipient: outputAccount, Date: time.Now(), ID: inputID}
		credit := classes.Transaction{Name: "Transfer from " + inputAccount, Amount: amount, Recipient: outputAccount, Date: time.Now(), ID: outputID}

		input.Transactions = append(input.Transactions, debit)
		output.Transactions = append(output.Transactions, credit)

		utils.UpdateAccount(input)
		utils.UpdateAccount(output)
	},
}

func init() {
	addTransactionCmd.Flags().BoolVarP(&piggyBankPayee, "piggyBank", "p", false, "Specify that the payee of this transaction is a piggy bank")

	editTransactionCmd.Flags().UintVarP(&transactionID, "transactionID", "i", 0, "ID of the transaction to modify")
	editTransactionCmd.Flags().BoolVarP(&newPiggyBankPayee, "piggyBank", "p", false, "Specify that the payee of this transaction is a piggy bank")
	editTransactionCmd.Flags().StringVar(&newName, "name", "", "New name of the transaction (optional)")
	editTransactionCmd.Flags().Float64Var(&newAmount, "amount", 0.0, "New amount of the transaction (optional)")
	editTransactionCmd.Flags().StringVar(&newPayee, "payee", "", "New payee of the transaction (optional)")

	deleteTransactionCmd.Flags().UintVarP(&transactionID, "transactionID", "i", 0, "ID of the transaction to modify")

	transferTransactionCmd.Flags().StringVarP(&inputAccount, "inputAccount", "i", "", "Account to debit from")
	transferTransactionCmd.Flags().StringVarP(&outputAccount, "outputAccount", "o", "", "Account to credit to")

	transactionCmd.AddCommand(viewTransactionsCmd, addTransactionCmd, editTransactionCmd, deleteTransactionCmd, transferTransactionCmd)
	rootCmd.AddCommand(transactionCmd)
}
