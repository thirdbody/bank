package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"gitlab.com/myl0g/bank/utils"
)

var account string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "bank",
	Short: "Manage accounts, transactions, and piggy banks.",
	Long: `A utility to manage a financial record.
  
  Set the BANK_CONFIG_DIR environment variable to use a non-standard config directory (must end with a slash).`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	if _, err := os.Stat(utils.GetConfigDir()); os.IsNotExist(err) {
		// Config dir does not exist; create it
		utils.Initialize()
	}

	rootCmd.PersistentFlags().StringVarP(&account, "account", "a", utils.GetConfig().DefaultAccount, "Specify the account to use with the operation (default can be set with bank setDefault).")
}
