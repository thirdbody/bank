package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/myl0g/bank/classes"
	"gitlab.com/myl0g/bank/utils"
)

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:     "init [accounts...]",
	Short:   "Create a bank with accounts you provide.",
	Aliases: []string{"i"},
	Long: `Initialize this application with the accounts you specify.
	
	Example: "bank i Checking Savings" will initialize bank with the Checking and Savings accounts.`,

	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		// Create accounts
		err := os.Mkdir(utils.GetAccountsDir(), 0700)
		utils.Check(err)

		for _, account := range args {
			utils.UpdateAccount(classes.Account{Name: account, Transactions: []classes.Transaction{}})
		}
		utils.UpdateConfig(classes.Config{DefaultAccount: ""})

		fmt.Println("Initialized bank on this computer.")
	},
}

func init() {
	rootCmd.AddCommand(initCmd)
}
